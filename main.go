package main

import (
	"net/http"
)

func main() {
	http.HandleFunc("/", func(rw http.ResponseWriter, r *http.Request) {
		rw.Header().Add("content-type", "application/json")
		rw.Write([]byte(`{"products":[{"product_id":"1","name":"Rice With Bean"},{"product_id":"2","name":"Bean"}]}`))
	})
	panic(http.ListenAndServe("0.0.0.0:8080", nil))
}
