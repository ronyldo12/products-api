#!/bin/bash
export $(grep -v '^#' .env | xargs)

git add --all
git commit -m "commitando diferenças"

export CI_COMMIT_SHA=$(git rev-parse --verify HEAD)

docker build -t ronyldo12/products-api:$CI_COMMIT_SHA .
docker build -t ronyldo12/products-api:latest .

docker login -u ronyldo12 -p $DOCKERHUB_PASS

docker push ronyldo12/products-api:$CI_COMMIT_SHA

docker push ronyldo12/products-api:latest


cd k8s

kustomize edit set image IMAGE10=ronyldo12/products-api:$CI_COMMIT_SHA


git add --all
git commit -m "deploy"
git push origin main